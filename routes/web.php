<?php
use App\Image;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/images-all', 'ImagesController@index');
Route::post('/images-upload', 'ImagesController@upload');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
