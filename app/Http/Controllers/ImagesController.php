<?php

namespace App\Http\Controllers;
use App\Image;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Http\Request;

class ImagesController extends Controller
{

    public function index()
    {
        return  Image::where('user_id', Auth::id())->get();

    }

    public function upload(Request $request)
    {

        $user_id = Auth::id();
        $size = $request->size;
        $type = $request->type;
        $_image = $request->image;
        $hash = Str::random(16);

        $image = new Image();
        $image->user_id = $user_id;

        if($type == 'image/jpeg'){
            $filename = $hash.'.jpg';
            $folder = 'gallery/jpeg/';
            $_image->move($folder, $filename);

        }else{
            $filename = $hash.'.png';
            $folder = 'gallery/png/';
            $_image->move($folder, $filename);
        }


        $image->path = $folder.$filename;
        $image->size = $size;
        $image->type = $type;
        $image->save();

        return response()->json([
            "message" => $image
        ]);
    }
}
